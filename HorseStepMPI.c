#include <stdio.h>
#include "mpi.h"
#include <malloc.h>

#define N 8
void fillBoard(int index, int*** boards)
{
	int i;
	boards[index] = (int**)realloc(boards[index], N * sizeof(int*));
	for (i = 0; i < N; i++)
	{
		boards[index][i] = (int*)realloc(boards[index][i], N * sizeof(int));
	}
}
void printBoard(int index, int*** boards)
{
	int i, j;
	printf("\n");
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			printf(" %d ", boards[index][i][j]);
		}
		printf("\n\n");
	}
}
int checkClamp(int n, int max)
{
	return n >= 0 && n < max;
}
void reset(int*** boards)
{
	int i, j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			boards[0][i][j] = 0;
		}
	}
	boards[0][0][0] = 1;
}
void resizeArray(int boardsCount, int*** boards) 
{
	boards = (int***)realloc(boards, boardsCount * sizeof(int**));
}
void step(int* boardsCount, int*** boards, int steps[8][2], int*** result)
{
	int count = 0;
	int i, j, k, l;
	for (i = 0; i < *boardsCount; i++)
	{
		int max = 1;
		int x, y;
		for (j = 0; j < N; j++)
		{
			for (k = 0; k < N; k++)
			{
				if (boards[i][j][k] > max)
				{
					max = boards[i][j][k];
					x = j;
					y = k;
				}
			}
		}
		for (j = 0; j < 8; j++)
		{
			if (checkClamp(x + steps[j][0], N) &&
				checkClamp(y + steps[j][1], N) &&
				boards[i][x + steps[j][0]][y + steps[j][1]] == 0)
			{
				count++;
				resizeArray(count, result);
				fillBoard(count - 1, result);
				for (k = 0; k < N; k++)
				{
					for (l = 0; l < N; l++)
					{
						result[count - 1][k][l] = boards[i][k][l];
					}
				}
				result[count - 1][x][y] = max + 1;
			}
		}
	}
	*boardsCount = count;
}


int main(int arg, char** args)
{
	int rank, size;
	MPI_Init(&arg, &args);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status;
	int steps[8][2] = { 1, 2, 1, -2, -1, 2, -1, -2, 2, 1, -2, 1, 2, -1, -2, -1 };

		//MPI_Send(&a, 3, MPI_INT, 1, 10, MPI_COMM_WORLD);
		//MPI_Recv(&a, 3, MPI_INT, 1, 10, MPI_COMM_WORLD, &status);
	int boardsCount = 1;
	int*** boards = NULL;
	if (rank == 0) 
	{ 
		resizeArray(boardsCount, boards);
		fillBoard(0, boards);
		reset(boards);
		printBoard(0, boards);
	}

	if (rank == 0)
	{
		int iteration = 0;
		while (iteration < N * N - 1)
		{
			int i, j, k, l;
			int counts[size - 1];
			int ost = boardsCount % (size - 1);

			for (i = 0; i < (size - 1); i++)
			{
				counts[i] = boardsCount / (size - 1);
				if (ost > 0)
				{
					ost--;
					counts[i]++;
				}
			}

			int counter = 0;
			for (i = 0; i < size - 1; i++)
			{
				int*** array;
				resizeArray(counts[i], array);
				for (i = 0; i < counts[i]; i++)
				{
					fillBoard(i, array);
					for (j = 0; j < N; j++)
					{
						for (k = 0; k < N; k++)
						{
							array[i][j][k] = boards[counter + i][j][k];
						}
					}
				}
				counter += counts[i];
				MPI_Send(&counts[i], 1, MPI_INT, i + 1, 1, MPI_COMM_WORLD);
				MPI_Send(&array, counts[i] * N * N, MPI_INT, i + 1, 2, MPI_COMM_WORLD);
			}

			int**** allBoards;
			boardsCount = 0;
			int allCount[size - 1];
			allBoards = (int****)malloc((size - 1) * sizeof(int***));
			for (i = 0; i < size - 1; i++)
			{
				MPI_Recv(&(allCount[i]), 1, MPI_INT, i + 1, 1, MPI_COMM_WORLD, &status);
				resizeArray(allCount[i], allBoards[i]);
				for (j = 0; j < allCount[i]; j++)
				{
					fillBoard(j, allBoards[i]);
				}
				MPI_Recv(&(allBoards[i]), allCount[i] * N * N, MPI_INT, i + 1, 2, MPI_COMM_WORLD, &status);
				boardsCount += allCount[i];
				resizeArray(boardsCount, boards);
				for (j = 0; j < allCount[i]; j++)
				{
					for (k = 0; k < N; k++)
					{
						for (l = 0; l < N; l++)
						{
							boards[boardsCount - allCount[i] + j][k][l] = allBoards[i][j][k][l];
						}
					}
				}
			}
		}
		printBoard(0, boards);
	}
	if (rank != 0)
	{
		int i;
		MPI_Recv(&boardsCount, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
		resizeArray(boardsCount, boards);
		for (i = 0; i < boardsCount; i++)
		{
			fillBoard(i, boards);
		}
		MPI_Recv(&boards, boardsCount * N * N, MPI_INT, 0, 2, MPI_COMM_WORLD, &status);
		int*** result = NULL;
		step(&boardsCount, boards, steps, result);
		MPI_Send(&boardsCount, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		MPI_Send(&result, boardsCount * N * N, MPI_INT, 0, 2, MPI_COMM_WORLD);
	}
	MPI_Finalize();
	return 0;
}