#include <stdio.h>

int step(int N, int board[N][N], int *x, int *y, int dx, int dy, int stepNum)
{
	if (!checkClamp(*x + dx, N) || !checkClamp(*y + dy, N) || board[*x + dx][*y + dy] != -1) return 0;
	board[*x + dx][*y + dy] = stepNum + 1;
	*x += dx;
	*y += dy;
	return 1;
}


int goToPreviousPoint(int N, int board[N][N], int *x, int *y, int steps[8][2])
{
	int i;
	for (i = 0; i < 8; i++)
	{
		if (checkClamp(*x - steps[i][0], N) && checkClamp(*y - steps[i][1], N) && board[*x - steps[i][0]][*y - steps[i][1]] == board[*x][*y] - 1)
		{
			board[*x][*y] = -1;
			*x -= steps[i][0];
			*y -= steps[i][1];
			return i + 1;
		}
	}
}

void printBoard(int N, int board[N][N])
{
	int i, j;
	printf("\n");
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			printf(" %d ", board[i][j]);
		}
		printf("\n\n");
	}
}

int checkClamp(int n, int max)
{
	return n >= 0 && n < max;
}

void resetBoard(int N, int board[N][N], int *x, int *y)
{
	int i, j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			board[i][j] = -1;
		}
	}
	board[N-1][0] = 0;
	*x = N-1; 
	*y = 0;
}

void printArray(int N, int array[N])
{
	int i = 0;
	for (i = 0; i < N; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

int main(int arg, char** args)
{
	int rank, size;
	int N = 8;
	int board[N][N];
	int x, y, stepNum = 0, stepIndex = 0;
	int steps[8][2] = { 1, 2, 1, -2, -1, 2, -1, -2, 2, 1, -2, 1, 2, -1, -2, -1 };
	resetBoard(N, board, &x, &y);
	printBoard(N, board);
	while (stepNum < N * N - 1)
	{
		if (step(N, board, &x, &y, steps[stepIndex][0], steps[stepIndex][1], stepNum))
		{
			stepNum++;
			stepIndex = 0;
		}
		else
		{
			stepIndex++;
			while (stepIndex >= 8)
			{
				stepIndex = goToPreviousPoint(N, board, &x, &y, steps);
				stepNum--;

				if (stepNum < 0)
				{
					printf("��� ���� ������ ����������\n");
					return 1;
				}
			}
		}
	}
	printf("\n������:\n");
	printBoard(N, board);
	return 0;
}